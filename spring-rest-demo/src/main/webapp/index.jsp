<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPRING REST DEMO</title>
</head>
<body>
	<h2>SPRING REST DEMO</h2>
	
	<a href="${pageContext.request.contextPath}/test/hello">Hello</a>
	<hr>
	<a href="${pageContext.request.contextPath}/api/students">Students</a>
</body>
</html>